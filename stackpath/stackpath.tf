variable "stackpath_stack" {}
variable "stackpath_client_id" {}
variable "stackpath_client_secret" {}
variable "credentials_username" {}
variable "credentials_password" {}

provider "stackpath" {
  stack_id      = var.stackpath_stack
  client_id     = var.stackpath_client_id
  client_secret = var.stackpath_client_secret
}

resource "stackpath_compute_workload" "client" {
  name = "client"
  slug = "client"

  annotations = {
    # remove line completely to avoid anycast IP
    "anycast.platform.stackpath.net" = "true"
  }

  network_interface {
   network = "default"
  }

  image_pull_credentials {
    docker_registry {
      username = var.credentials_username
      password = var.credentials_password
    }
  }

  container {

    name = "r"
    image = "marcelwiget/mycrpd:latest"

    resources {
      requests = {
        "cpu" = "1"
        "memory" = "2Gi"
      }
    }

    env {
      key = "NAME1"
      value = "set to whatever you need"
    }

    # The ports that should be publicly exposed on the VM.
    port {
      name = "ssh"
      port = 22
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "bgp"
      port = 179
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "netconf"
      port = 830
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
  }

  target {
    name = "world"
    min_replicas = 1
    max_replicas = 1
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "FRA", "LAX", "LHR", "NRT"  # Frankfurt, Los Angeles, London, Tokyo
      ]
    }
  }
}

output "instances" {
  value = {
    for instance in stackpath_compute_workload.client.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "instance_ips" {
  value = ["${stackpath_compute_workload.client.instances.*.external_ip_address}"]
}

output "anycast_ip" {
  value = replace(lookup(stackpath_compute_workload.client.annotations, "anycast.platform.stackpath.net/subnets", ""), "/32", "")
}
