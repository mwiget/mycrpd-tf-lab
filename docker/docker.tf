# Configure the Docker provider
provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "mycrpd" {
  name = "mycrpd:latest"
  keep_locally = true
}

# Start a container
resource "docker_container" "r1" {
  name  = "r1"
  image = docker_image.mycrpd.latest

  capabilities {
    add  = ["ALL"]
  }

  ports {
    internal = 22
    external = 5001
  }

}
resource "docker_container" "r2" {
  name  = "r2"
  image = docker_image.mycrpd.latest

  capabilities {
    add  = ["ALL"]
  }
  ports {
    internal = 22
    external = 5002
  }

}
resource "docker_container" "r3" {
  name  = "r3"
  image = docker_image.mycrpd.latest

  capabilities {
    add  = ["ALL"]
  }
  ports {
    internal = 22
    external = 5003
  }
}

output "instances" {
  value =  {
    "r1" =  {
     "phase": "RUNNING"
     "private_ip" = lookup(docker_container.r1.network_data[0], "ip_address")
     "public_ip" = lookup(docker_container.r1.network_data[0], "ip_address")
    }
    "r2" =  {
     "phase": "RUNNING"
     "private_ip" = lookup(docker_container.r2.network_data[0], "ip_address")
     "public_ip" = lookup(docker_container.r2.network_data[0], "ip_address")
    }
    "r3" =  {
     "phase": "RUNNING"
     "private_ip" = lookup(docker_container.r3.network_data[0], "ip_address")
     "public_ip" = lookup(docker_container.r3.network_data[0], "ip_address")
    }
  }
}
