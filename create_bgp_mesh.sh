#!/bin/bash

provider=${1:-docker}

SECONDS=0

while true; do
  echo "waiting for all instances to be ready ..."
  sleep 2
  $provider/show-ip.sh | grep \"phase\": | grep -v RUNNING || break;
done

$provider/show-ip.sh | python3 render_push_commit.py

echo "$0 completed in $SECONDS seconds"
echo ""
