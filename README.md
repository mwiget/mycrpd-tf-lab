# Juniper cRPD Terraform lab

[[_TOC_]]

# Overview

Purpose of this repo is to demonstrate [Juniper cRPD](https://www.juniper.net/documentation/product/en_US/crpd) 
container deployed via [Terraform](https://www.terraform.io/) using two different providers, docker and 
Stackpath and configure the running instances to form a full mesh via automation (Jinja2 & PyEz). 

The topology in Stackpath Edge Cloud will look like this (only 3 regions with one instance each shown, but it
can be scaled up to any number of regions with any number of cRPD instances):

```mermaid
graph LR
subgraph stackpath["SP// Edge Compute Anycast IP"]
style stackpath fill:#9cf
subgraph "region1"
style region1 fill:#9fc
r1
end
subgraph "region2"
style region2 fill:#fc9
r2
end
subgraph "region3"
style region3 fill:#f99
r3
end
r1 ---|BGP| r2
r1 ---|BGP| r3
r2 ---|BGP| r3
end
```

To launch cRPD instances with ssh/netconf active using a ssh key, license key and a minimal junos configuration 
requires prepping the container before launch. This step is completed first and proper function can be checked
locally with docker, prior to moving on to more complex steps involving Terraform.

# Requirements

- Docker: Follow instructions at https://docs.docker.com/engine/install/ubuntu/ or use https://www.docker.com/products/docker-desktop on Windows and OS/X.
- Terraform: Follow instructions at https://learn.hashicorp.com/tutorials/terraform/install-cli#install-terraform
- `git` and `make` (if not already present), e.g. on Ubuntu, use `sudo apt-get install git make`.
- Python3 modules jxmlease, lxml and junos-eznc. install with `sudo pip3 install jxmlease lxml junos-eznc`
- `jq`: command-line JSON processor. Install with `sudo apt-get install jq`
- Juniper cRPD from the Juniper download site or using https://www.juniper.net/us/en/dm/crpd-trial/ .
- cRPD license key (can be requested via https://www.juniper.net/us/en/dm/crpd-trial/ as well).
- Local clone of this repo

# Preparation

## Clone this repo

to your working directory and change into the created folder:

```
$ git clone https://gitlab.com/mwiget/mycrpd-tf-lab
$ cd mycrpd-tf-lab
```

## Download and load Juniper cRPD

from the official Juniper Software Download site at https://support.juniper.net/support/downloads/ .
Depending on your access credentials, select either cRPD or 'cRPD Evaluation'. Pick the newest version
available to you, as it provides the richest feature set.

Import the downloaded tgz file into Docker (pick the version you downloaded):

```
$ docker load -i junos-routing-crpd-docker-20.2R1.10.tgz
```

## cRPD license key

Required to use any cRPD features, including BGP. If you don't own one, you can get a trial license from https://www.juniper.net/us/en/dm/crpd-trial/.
Download the eval license into the file mycrpd/junos_sfnt.lic . 

# Augment cRPD container

We want to deploy the container ready with a newly generated ssh public key for easy ssh and netconf
access. To deploy the image later in a public cloud, e.g. Stackpath, we also package a minimal Junos config
along. For fun, the login banner message is enhanced with resource information available to cRPD at runtime.

For the impatient, simply run 'make' and you will be greeted with a cRPD shell after a while. The same
steps are described and executed here:

## Create ssh public/private keypair

```
$ ssh-keygen -f mycrpd/mycrpd.key
$ ls -l mycrpd/mycrpd.key*
-rw------- 1 mwiget mwiget 2602 Aug  6 20:41 mycrpd/mycrpd.key
-rw-r--r-- 1 mwiget mwiget  566 Aug  6 20:41 mycrpd/mycrpd.key.pub
```

The public key will be added into the container, while the the private key will be used later for ssh/netconf
access to the running cRPD instance.

## Build the container

Builds the container according to the instructions found in mycrpd/Dockerfile:
```
$ docker build -t mycrpd mycrpd/
. . .
Successfully built 3a37b89b4f30
Successfully tagged mycrpd:latest
```

## launch mycrpd locally

Launch the newly built crpd:latest container as a daemon, then launch a bash shell in it:

```
$ docker run -t --rm -d --name mycrpd mycrpd:latest
$ docker exec -ti mycrpd bash

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===

 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC

               _/_/_/    _/_/_/    _/_/_/
      _/_/_/  _/    _/  _/    _/  _/   _/   cb6a2c859dc8 (172.17.0.2)
   _/        _/_/_/    _/_/_/    _/    _/  Cores 0-3, 3822 MBytes free
  _/        _/    _/  _/        _/   _/  Intel(R) Celeron(R) J4105 CPU @ 1.50GHz
   _/_/_/  _/    _/  _/        _/_/_/

root@cb6a2c859dc8:~# cli
root@cb6a2c859dc8> show interfaces routing
Interface        State Addresses
lo.0             Up    ISO   enabled
eth0             Up    ISO   enabled
                       INET  172.17.0.2

root@cb6a2c859dc8> show system license 
License usage: 
                                 Licenses     Licenses    Licenses    Expiry
  Feature name                       used    installed      needed 
  containerized-rpd-standard            0            1           0    2020-12-01 23:59:00 UTC

Licenses installed: 
  License identifier: fbf9f46f-d167-42af-8dda-f469e523fe79
  License SKU: JNX_LICFEAT_CRPD_STANDARD
  License version: 1
  Order Type: commercial
  Customer ID: |**CustomerId**|
  License count: 1
  Features:
    containerized-rpd-standard - Containerized routing protocol daemon with standard features
      date-based, 2020-07-01 00:00:00 UTC - 2020-12-01 23:59:00 UTC

root@cb6a2c859dc8> exit

root@cb6a2c859dc8:~# quit
```

Lets try out ssh (user root) access using the previously created ssh key:

```
$ ssh -i mycrpd.key root@172.17.0.2 cli show route
The authenticity of host '172.17.0.2 (172.17.0.2)' can't be established.
ECDSA key fingerprint is SHA256:3LVWJeDsm1LZMmCQizUg1k0PFvUbsfIOu7EvHiyBvKE.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.17.0.2' (ECDSA) to the list of known hosts.

inet.0: 2 destinations, 2 routes (2 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

172.17.0.0/16      *[Direct/0] 00:02:46
                    >  via eth0
172.17.0.2/32      *[Local/0] 00:02:46
                       Local via eth0

inet6.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

ff02::2/128        *[INET6/0] 00:02:46
                       MultiRecv
```

Terminate the running instance with

```
$ docker stop mycrpd
```

The [Makefile](Makefile) can be used to build and run mycrpd locally via `make run`. This will build and run
the container as a daemon, then start a bash shell in it. Once the user exists the shell, the container is 
destroyed. Nice method to quickly test out changes done to the container. 

## Description of the various files used to build mycrpd

To understand what has just happened, check the content of folder [mycrpd/](mycrpd/) and start with the 
[mycrpd/Dockerfile](mycrpd/Dockerfile). 

```dockerfile
$ cat mycrpd/Dockerfile
# Adjust FROM according to your downloaded cRPD version
FROM crpd:20.2R1.10
#FROM hub.juniper.net/routing/crpd:19.4R1.10

# Adding tcpdump and getting rid of cached package lists

RUN apt-get update \
  && apt-get -y --no-install-recommends install tcpdump \
  && rm -rf /var/lib/apt/lists/*

# augment the container with sshd config for ssh and netconf and add
# our ssh public key for access it at runtime

COPY sshd_config /etc/ssh
COPY mycrpd.key.pub /root/.ssh/authorized_keys

# augment the startup script to execute our own script. Typically, such
# script is mounted at runtime, but here everything gets packaged into
# the container

COPY runit-init.sh /sbin/
COPY network-init.sh /config/
RUN chmod +x /sbin/runit-init.sh /config/network-init.sh \
  && chmod 600 /root/.ssh/authorized_keys

# copy cRPD license key into its required location. Similar to the networking
# script, the folder /config is typically mounted to provide the key and junos
# config at runtime

COPY junos_sfnt.lic /config/license/safenet/
COPY juniper.conf /config/

WORKDIR /root
# SIGRTMIN+3  ## using this leads to zombies in SP//, better use 'STOPSIGNAL 35'
STOPSIGNAL 35
```

It starts from the official crpd container via the FROM line. Adjust
the version accordingly to your download. Then tcpdump gets installed, the sshd_config and ssh public key 
files copied into the correct locations, followed by an augmented version of 
[mycrpd/runit-init.sh](mycrpd/runit-init.sh). This file gets executed first at container instantiation and 
is used to augment the content of /etc/version, which gets displayed when /bin/bash is launched, e.g. 
`docker exec` or ssh into the running instances. Finally, it launches [mycrpd/network-init.sh](mycrpd/network-init.sh), which can be used to configure anything related to the Linux namespaces the container will run in. 
Such a file can be loaded e.g. at runtime from a mounted volume. An example use case can be found in this 
repo: https://gitlab.com/mwiget/crpd-l2tpv3-xdp . In this demo, there is no need for any special settings, 
hence it simply launches `ifconfig` and terminates. 

A word about [mycrpd/sshd_config](mycrpd/sshd_config): It allows access via TCP ports 22 and 830 and configures
the subsystem netconf to launch `/usr/bin/netconf`. This will be used to run PyEz against cRPD over SSH.

# Terraform

[Terraform](https://www.terraform.io/) uses Infrastructure as Code to provision and manage any cloud, 
infrastructure, or service. Where things get provisioned is selected via providers. There are great tutorials
and documentation available on the Terraform website: https://www.terraform.io/docs/index.html .

Here, we will use Terraform to provision a few cRPD instances locally in 
[Docker](https://www.terraform.io/docs/providers/docker/index.html), follwed by launching instances
in the public cloud provided by [Stackpath Provider](https://registry.terraform.io/providers/stackpath/stackpath/latest/docs).


## Using Docker Provider

Terraform support many plugins, aka providers, which drive various runtime engines. To get a first taste,
the Docker provider module is used to launch a few cRPD instances locally in Docker.

The definition or plan what to do can be found in [local.tf](local.tf):

```terraform
$ cat docker/docker.tf
# Configure the Docker provider
provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "mycrpd" {
  name = "mycrpd:latest"
  keep_locally = true
}

# Start a container
resource "docker_container" "r1" {
  name  = "r1"
  image = docker_image.mycrpd.latest

  capabilities {
    add  = ["ALL"]
  }

  ports {
    internal = 22
    external = 5001
  }

}
resource "docker_container" "r2" {
  name  = "r2"
  image = docker_image.mycrpd.latest

  capabilities {
    add  = ["ALL"]
  }
  ports {
    internal = 22
    external = 5002
  }

}
resource "docker_container" "r3" {
  name  = "r3"
  image = docker_image.mycrpd.latest

  capabilities {
    add  = ["ALL"]
  }
  ports {
    internal = 22
    external = 5003
  }
}

output "instances" {
  value =  {
    "r1" =  {
     "phase": "RUNNING"
     "private_ip" = lookup(docker_container.r1.network_data[0], "ip_address")
     "public_ip" = lookup(docker_container.r1.network_data[0], "ip_address")
    }
    "r2" =  {
     "phase": "RUNNING"
     "private_ip" = lookup(docker_container.r2.network_data[0], "ip_address")
     "public_ip" = lookup(docker_container.r2.network_data[0], "ip_address")
    }
    "r3" =  {
     "phase": "RUNNING"
     "private_ip" = lookup(docker_container.r3.network_data[0], "ip_address")
     "public_ip" = lookup(docker_container.r3.network_data[0], "ip_address")
    }
  }
}
```

The output stanza will be used at runtime to extract the assigned IP address(es). In this simple case running cRPD in docker using a single network interface,
this seems an overkill to show the same IP as private and public, so is the hard-coded 'RUNNING' phase. This information makes the output compatible with the 
automation script that provisions the BGP full mesh and its content will be correct when using the Stackpath Terraform provider.

With Terraform installed locally (see above), initialize it as follows:

```
$ terraform init
```

This will download provider.docker if needed and initialize it. It knows about the need for docker from the local.tf file provider 
definition. If your local docker engine can't be reached at the spefified linux socket, adjust accordingly. 

To check what terraform will do, use

```
$ terraform plan

Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.


------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # docker_container.r1 will be created
  + resource "docker_container" "r1" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = (known after apply)
      + container_logs   = (known after apply)
      + dns              = (known after apply)
      + dns_opts         = (known after apply)
      + entrypoint       = (known after apply)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = (known after apply)
      + log_opts         = (known after apply)
      + logs             = false
      + must_run         = true
      + name             = "r1"
      + network_data     = (known after apply)
      + read_only        = false
      + restart          = "no"
      + rm               = false
      + shm_size         = (known after apply)
      + start            = true
      + user             = (known after apply)
      + working_dir      = (known after apply)

      + capabilities {
          + add  = [
              + "ALL",
            ]
          + drop = []
        }
    }

  # docker_container.r2 will be created
  + resource "docker_container" "r2" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = (known after apply)
      + container_logs   = (known after apply)
      + dns              = (known after apply)
      + dns_opts         = (known after apply)
      + entrypoint       = (known after apply)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = (known after apply)
      + log_opts         = (known after apply)
      + logs             = false
      + must_run         = true
      + name             = "r2"
      + network_data     = (known after apply)
      + read_only        = false
      + restart          = "no"
      + rm               = false
      + shm_size         = (known after apply)
      + start            = true
      + user             = (known after apply)
      + working_dir      = (known after apply)

      + capabilities {
          + add  = [
              + "ALL",
            ]
          + drop = []
        }
    }

  # docker_container.r3 will be created
  + resource "docker_container" "r3" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = (known after apply)
      + container_logs   = (known after apply)
      + dns              = (known after apply)
      + dns_opts         = (known after apply)
      + entrypoint       = (known after apply)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = (known after apply)
      + log_opts         = (known after apply)
      + logs             = false
      + must_run         = true
      + name             = "r3"
      + network_data     = (known after apply)
      + read_only        = false
      + restart          = "no"
      + rm               = false
      + shm_size         = (known after apply)
      + start            = true
      + user             = (known after apply)
      + working_dir      = (known after apply)

      + capabilities {
          + add  = [
              + "ALL",
            ]
          + drop = []
        }
    }

  # docker_image.mycrpd will be created
  + resource "docker_image" "mycrpd" {
      + id           = (known after apply)
      + keep_locally = true
      + latest       = (known after apply)
      + name         = "mycrpd:latest"
    }

Plan: 4 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.

```

Now lets execute with 'terraform apply'. It usually asks for confirmation, which we bypass with the '-auto-approve':

```
$ terraform apply

docker_image.mycrpd: Creating...
docker_image.mycrpd: Creation complete after 0s [id=sha256:1ce6af09ab6bd9779e2f52403ccba4105a7baaf2ed80dd61ddb5d51cc4ae864emycrpd:latest]
docker_container.r3: Creating...
docker_container.r1: Creating...
docker_container.r2: Creating...
docker_container.r1: Creation complete after 1s [id=f3d57e3d5bdab4bf073c3995b7e48995d4a8d3bf4e775c9133625b3ed9958574]
docker_container.r3: Creation complete after 1s [id=2c8d28e69570f9c5134bb267c9ca1a93e06301119c47c73341763456764bcf92]
docker_container.r2: Creation complete after 1s [id=f839e94947efb23563bcb5702fc42e104a511c7970f3ed4de5ba95c7c5467db8]

Apply complete! Resources: 4 added, 0 changed, 0 destroyed.

Outputs:

ip_addresses = {
  "r1" = "172.17.0.2"
  "r2" = "172.17.0.4"
  "r3" = "172.17.0.3"
}
```

We can verify the containers using docker directly:

```
$ docker ps 
CONTAINER ID        IMAGE               COMMAND                 CREATED              STATUS              PORTS                                                                         NAMES
f839e94947ef        1ce6af09ab6b        "/sbin/runit-init.sh"   About a minute ago   Up About a minute   22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp, 50051/tcp   r2
2c8d28e69570        1ce6af09ab6b        "/sbin/runit-init.sh"   About a minute ago   Up About a minute   22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp, 50051/tcp   r3
f3d57e3d5bda        1ce6af09ab6b        "/sbin/runit-init.sh"   About a minute ago   Up About a minute   22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp, 50051/tcp   r1
```

And execute a junos cli command first using 'docker exec', then also using the instances ip address with our previously created ssh key:

```
$ docker exec -ti r1 cli show interface routing eth0
Interface        State Addresses
eth0             Up    ISO   enabled
                       INET  172.17.0.2
```

and now via ssh:

```
$ ssh -i mycrpd.key root@172.17.0.2 cli show interface routing eth0
Interface        State Addresses
eth0             Up    ISO   enabled
                       INET  172.17.0.2
```

We can log into one of the instances via ssh, launch cli and work interactively:

```
$ ssh -i mycrpd.key root@172.17.0.3

The authenticity of host '172.17.0.3 (172.17.0.3)' can't be established.
ECDSA key fingerprint is SHA256:3LVWJeDsm1LZMmCQizUg1k0PFvUbsfIOu7EvHiyBvKE.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.17.0.3' (ECDSA) to the list of known hosts.

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===

 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC

               _/_/_/    _/_/_/    _/_/_/
      _/_/_/  _/    _/  _/    _/  _/   _/   2c8d28e69570 (172.17.0.3)
   _/        _/_/_/    _/_/_/    _/    _/  Cores 0-3, 540 MBytes free
  _/        _/    _/  _/        _/   _/  Intel(R) Celeron(R) J4105 CPU @ 1.50GHz
   _/_/_/  _/    _/  _/        _/_/_/

root@2c8d28e69570:~# cli
root@2c8d28e69570> show route 

inet.0: 2 destinations, 2 routes (2 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

172.17.0.0/16      *[Direct/0] 00:06:12
                    >  via eth0
172.17.0.3/32      *[Local/0] 00:06:12
                       Local via eth0

inet6.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

ff02::2/128        *[INET6/0] 00:06:12
                       MultiRecv

root@2c8d28e69570>  quit

root@2c8d28e69570:~# exit
```

## Using Stackpath Provider

This requires a bit of preparation work:

- Stackpath client id and secret
- Container registry credentials to upload and access the locally built mycrpd container
- Create stackpath/terraform.tfvars with stackpath and container registry credentials
- Upload mycrpd to your container registry

### Stackpath stack and client id

Create an account with https://www.stackpath.com/ (requires a credit card). Once logged in, go to `API Management' 
and create Credentials. Copy the file terraform.tfvars.sample to terraform.tfvars in the folder stackpath and
update the variables stackpath_client_id and stackpath_client_secret according to the newly generated credentials.

```
$ cd stackpath
$ cp terraform.tfvars.sample terraform.tfvars
```

To fill in the variable stackpath_stack in the same file, go to 'Stacks' on the stackpath web page (must be logged in) and copy the content of 'Slug'. Unless you created a new stack, this will be similar to 'my-default-stack-xxxxx'.

### Container registry credentials

If you don't have access to a private container registry, create a free account with https://hub.docker.com/.
This will give you one private registry to use for this use case. Place the registry's credentials_username and
credentials_password into the terraform.tfvars. 

The final terraform variable file will have these entries populated:

```
$ cat stackpath/terraform.tvars

stackpath_stack = "my-default-stack-....."
stackpath_client_id = "................................"
stackpath_client_secret = "................................................................"
credentials_username = "..........."
credentials_password = "................"
```

### Upload mycrpd

Build the local mycrpd container (as described earlier), then log into docker hub from the CLI, tag and push
the container. in this example, I'm using the userid marcelwiget:

```
$ docker images | grep mycrpd
mycrpd      latest          4f4b4038b669      1 hour ago      243MB

$ docker tag mycrpd:latest marcelwiget/mycrpd:latest
$ docker login 
$ docker push marcelwiget/mycrpd:latest
. . . 
latest: digest: sha256:adeff5ac66501de08523ff416519cc885f6aad6b98d9a318a3141f72f35826c6 size: 7170
```

### stackpath/stackpath.tf

The declarative description of what we want to launch in Stackpath is defined in the file
[stackpath/stackpath.tf](stackpath/stackpath.tf). Check the provider documentation for stackpath at
https://registry.terraform.io/providers/stackpath/stackpath/latest/docs for details.

In this file, we ask to create instances in Frankfurt, Los Angeles, London and Tokyo using the 3-letter
city codes (FRA, LAX, LHR and NRT). Each region shall have exactly one replica instantiated. Stackpath 
allows dynamic scale up and down with predefined boundaries defined in this file. Here, we simply ask for exactly
one replica per region.

Port access policies are defined to allow bgp, ssh and netconf traffic reach each instances via their 
public IP addresses. As all instances, including cross-region, are connected to the same private subnet, they
always have full access between them. 

Don't forget to adjust the contianer image name to match your container registry name (replace marcelwiget/mycrpd:latest  with the full container tag pushed to docker hub.

An anycast IP address is requested by defining an annotation.

To learn the assigned anycast, public and private IP address of each instance, corresponding output stanzas
are defined in the file too.

### Launch crpd instances

Always good practice to initialize terraform (which will download and install the provider), then do a dry run
before launching the stack:

```
$ cd stackpath
$ terraform init
. . .
$ terraform plan
. . .
$ terraform apply

. . .

Plan: 1 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + instance_ips = [
      + (known after apply),
    ]
  + instances    = (known after apply)

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: 
stackpath_compute_workload.client: Creating...
stackpath_compute_workload.client: Creation complete after 3s [id=8f24d413-b1cf-4270-95ce-4875635ea1f8]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

Outputs:

anycast_ip = 185.85.196.31
instance_ips = [
  [],
]
instances = {}
```

Not much, right? All we got so far is the anycast ip. Use `terraform refresh` to get a progress report and repeat 
until all instances are in RUNNING state:

```
$ terraform refresh

stackpath_compute_workload.client: Refreshing state... [id=8f24d413-b1cf-4270-95ce-4875635ea1f8]

Outputs:

anycast_ip = 185.85.196.31
instance_ips = [
  [
    "151.139.71.17",
    "151.139.43.39",
    "151.139.87.12",
    "151.139.176.36",
  ],
]
instances = {
  "client-world-fra-0" = {
    "name" = "client-world-fra-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.0.2"
    "public_ip" = "151.139.87.12"
  }
  "client-world-lax-0" = {
    "name" = "client-world-lax-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.64.2"
    "public_ip" = "151.139.71.17"
  }
  "client-world-lhr-0" = {
    "name" = "client-world-lhr-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.80.2"
    "public_ip" = "151.139.43.39"
  }
  "client-world-nrt-0" = {
    "name" = "client-world-nrt-0"
    "phase" = "STARTING"
    "private_ip" = "10.128.96.2"
    "public_ip" = "151.139.176.36"
  }
}
```

Once they are up and running, you can log into any of them using either their public IP address or
anycast address:

```
$ ssh -i mycrpd.key root@151.139.43.39
The authenticity of host '151.139.43.39 (151.139.43.39)' can't be established.
ECDSA key fingerprint is SHA256:3LVWJeDsm1LZMmCQizUg1k0PFvUbsfIOu7EvHiyBvKE.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '151.139.43.39' (ECDSA) to the list of known hosts.

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===

 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-lhr-0 (10.128.80.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2581 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2620 v2 @ 2.10GHz
  _/_/_/  _/    _/  _/        _/_/_/

root@client-world-lhr-0:~# cli
root@client-world-lhr-0> show bgp summary 
Threading mode: BGP I/O
Groups: 1 Peers: 0 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                       0          0          0          0          0          0
inet6.0              
                       0          0          0          0          0          0

root@client-world-lhr-0> show interfaces routing 
Interface        State Addresses
lo.0             Up    ISO   enabled
                       INET  185.85.196.31
eth0             Up    ISO   enabled
                       INET  10.128.80.2

root@client-world-lhr-0> exit 

root@client-world-lhr-0:~# exit
logout
Connection to 151.139.43.39 closed.
```

And now using the anycast IP:

```
$ ssh -i mycrpd.key root@185.85.196.31

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===

 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-fra-0 (10.128.0.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2567 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2660 v4 @ 2.00GHz
  _/_/_/  _/    _/  _/        _/_/_/

root@client-world-fra-0:~# exit
logout
Connection to 185.85.196.31 closed.
```

As I'm working from Europe, anycast connected me to the closest region, which is Frankfurt.
Next step is to use automation to build the BGP mesh.

# Automation

## Building BGP mesh via Jinja2 and PyEz

This section applies to both providers used here and is executed, once the instances are launched. 
The script [create_bgp_mesh.sh](create_bgp_mesh.sh) first waits until terraform reports all instances running,
which can take several seconds on any public cloud, including Stackpath. Then the assigned private and public
IP addresses are queried via Terraform and used to render and push Jinja2 [templates/bgp.j2](templates//bgp.j2)

```
$ ./tf-show-local-ip.sh | python3 render_push_commit.py 
```

Launching the script right after 'terraform apply' results in this output:

```
$ ./create_bgp_mesh.sh stackpath

waiting for all instances to be ready ...
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
waiting for all instances to be ready ...
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
waiting for all instances to be ready ...
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
waiting for all instances to be ready ...
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
waiting for all instances to be ready ...
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
waiting for all instances to be ready ...
    "phase": "STARTING",
    "phase": "STARTING",
    "phase": "STARTING",
waiting for all instances to be ready ...

loading router ip addresses from stdin ...
rendering configs/client-world-fra-0.conf.txt ...
rendering configs/client-world-lax-0.conf.txt ...
rendering configs/client-world-lhr-0.conf.txt ...
rendering configs/client-world-nrt-0.conf.txt ...

push config change to devices ...
push config to client-world-fra-0 (151.139.87.12) ...

[edit routing-options]
+   static {
+       route 151.139.87.12/32 discard;
+   }
[edit protocols bgp group internal]
+     neighbor 10.128.64.2;
+     neighbor 10.128.80.2;
+     neighbor 10.128.96.2;

push config to client-world-lax-0 (151.139.71.17) ...

[edit routing-options]
+   static {
+       route 151.139.71.17/32 discard;
+   }
[edit protocols bgp group internal]
+     neighbor 10.128.0.2;
+     neighbor 10.128.80.2;
+     neighbor 10.128.96.2;

push config to client-world-lhr-0 (151.139.43.48) ...

[edit routing-options]
+   static {
+       route 151.139.43.48/32 discard;
+   }
[edit protocols bgp group internal]
+     neighbor 10.128.0.2;
+     neighbor 10.128.64.2;
+     neighbor 10.128.96.2;

push config to client-world-nrt-0 (151.139.176.36) ...

[edit routing-options]
+   static {
+       route 151.139.176.36/32 discard;
+   }
[edit protocols bgp group internal]
+     neighbor 10.128.0.2;
+     neighbor 10.128.64.2;
+     neighbor 10.128.80.2;

./create_bgp_mesh.sh completed in 62 seconds
```

Now the instances are provisioned with a full BGP mesh and we can log into any of them to check 
their learned routes. E.g. using their anycast IP (use `terraform show` from within the stackpath folder), or
any of their assigned public IP addresses:

```
$ stackpath/show-ip.sh
```

```
$ ssh -i mycrpd.key root@151.139.87.12 cli[K[K[Kbash -i

bash: cannot set terminal process group (-1): Inappropriate ioctl for device
bash: no job control in this shell

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===

 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-fra-0 (10.128.0.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2568 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2660 v4 @ 2.00GHz
  _/_/_/  _/    _/  _/        _/_/_/

root@client-world-fra-0:~# 

root@client-world-fra-0:~# ip a
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet 185.85.196.9/32 brd 185.85.196.9 scope global lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 0a:58:0a:80:00:02 brd ff:ff:ff:ff:ff:ff
    inet 10.128.0.2/20 brd 10.128.15.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::858:aff:fe80:2/64 scope link 
       valid_lft forever preferred_lft forever
root@client-world-fra-0:~# cli
cli
root@client-world-fra-0> show bgp summary
show bgp summary 
Threading mode: BGP I/O
Groups: 1 Peers: 3 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                       9          6          0          0          0          0
inet6.0              
                       0          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
10.128.64.2      4259905000         75         75       0       0       32:14 Establ
  inet.0: 2/3/3/0
  inet6.0: 0/0/0/0
10.128.80.2      4259905000         75         74       0       0       32:11 Establ
  inet.0: 2/3/3/0
  inet6.0: 0/0/0/0
10.128.96.2      4259905000         75         73       0       0       32:09 Establ
  inet.0: 2/3/3/0
  inet6.0: 0/0/0/0

root@client-world-fra-0> show route
show route 

inet.0: 10 destinations, 13 routes (10 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

10.128.0.0/20      *[Direct/0] 00:32:37
                    >  via eth0
10.128.0.2/32      *[Local/0] 00:32:37
                       Local via eth0
10.128.64.0/20     *[BGP/170] 00:32:18, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.64.2
10.128.80.0/20     *[BGP/170] 00:32:15, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.80.2
10.128.96.0/20     *[BGP/170] 00:32:12, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.96.2
151.139.43.48/32   *[BGP/170] 00:32:15, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.80.2
151.139.71.17/32   *[BGP/170] 00:32:18, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.64.2
151.139.87.12/32   *[Static/5] 00:32:29
                       Discard
151.139.176.36/32  *[BGP/170] 00:32:12, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.96.2
185.85.196.9/32    *[Direct/0] 00:32:37
                    >  via lo.0
                    [BGP/170] 00:32:18, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.64.2
                    [BGP/170] 00:32:15, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.80.2
                    [BGP/170] 00:32:12, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.96.2

inet6.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

ff02::2/128        *[INET6/0] 00:32:37
                       MultiRecv

root@client-world-fra-0> show interface routing
show interfaces routing 
Interface        State Addresses
lo.0             Up    ISO   enabled
                       INET  185.85.196.9
eth0             Up    ISO   enabled
                       INET  10.128.0.2

root@client-world-fra-0> quit
quit 

root@client-world-fra-0:~# exit
exit
```

```
$ ssh -i mycrpd.key root@151.139.87.12 bash -i
The authenticity of host '151.139.176.36 (151.139.176.36)' can't be established.
ECDSA key fingerprint is SHA256:3LVWJeDsm1LZMmCQizUg1k0PFvUbsfIOu7EvHiyBvKE.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '151.139.176.36' (ECDSA) to the list of known hosts.

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===

 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-nrt-0 (10.128.96.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2582 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2620 v2 @ 2.10GHz
  _/_/_/  _/    _/  _/        _/_/_/

root@client-world-nrt-0:~# 
root@client-world-nrt-0:~# cli ip a
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet 185.85.196.9/32 brd 185.85.196.9 scope global lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 0a:58:0a:80:60:02 brd ff:ff:ff:ff:ff:ff
    inet 10.128.96.2/20 brd 10.128.111.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::858:aff:fe80:6002/64 scope link 
       valid_lft forever preferred_lft forever
root@client-world-nrt-0:~# cli
cli
root@client-world-nrt-0> show route
show route 

inet.0: 10 destinations, 13 routes (10 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

10.128.0.0/20      *[BGP/170] 00:33:12, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.0.2
10.128.64.0/20     *[BGP/170] 00:33:11, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.64.2
10.128.80.0/20     *[BGP/170] 00:33:11, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.80.2
10.128.96.0/20     *[Direct/0] 00:33:23
                    >  via eth0
10.128.96.2/32     *[Local/0] 00:33:23
                       Local via eth0
151.139.43.48/32   *[BGP/170] 00:33:11, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.80.2
151.139.71.17/32   *[BGP/170] 00:33:11, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.64.2
151.139.87.12/32   *[BGP/170] 00:33:12, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.0.2
151.139.176.36/32  *[Static/5] 00:33:11
                       Discard
185.85.196.9/32    *[Direct/0] 00:33:23
                    >  via lo.0
                    [BGP/170] 00:33:12, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.0.2
                    [BGP/170] 00:33:11, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.64.2
                    [BGP/170] 00:33:11, localpref 100
                      AS path: I, validation-state: unverified
                    >  to 10.128.80.2

inet6.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

ff02::2/128        *[INET6/0] 00:33:23
                       MultiRecv

root@client-world-nrt-0> show bgp summary
show bgp summary 
Threading mode: BGP I/O
Groups: 1 Peers: 3 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                       9          6          0          0          0          0
inet6.0              
                       0          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
10.128.0.2       4259905000         78         78       0       0       33:17 Establ
  inet.0: 2/3/3/0
  inet6.0: 0/0/0/0
10.128.64.2      4259905000         78         78       0       0       33:17 Establ
  inet.0: 2/3/3/0
  inet6.0: 0/0/0/0
10.128.80.2      4259905000         78         77       0       0       33:17 Establ
  inet.0: 2/3/3/0
  inet6.0: 0/0/0/0

root@client-world-nrt-0> show interface routing
show interfaces routing 
Interface        State Addresses
lo.0             Up    ISO   enabled
                       INET  185.85.196.9
eth0             Up    ISO   enabled
                       INET  10.128.96.2

root@client-world-nrt-0> quit
quit 

root@client-world-nrt-0:~# exit
exit
```

## Collect system info from every instance

Using the public Ip addresses returned by `terraform output`, we can query each instance,
 asking to show the content of /etc/version (which contains runtime data and is displayed when
 logging in via ssh). This script loops thru the IP's:

 ```
 $ cat stackpath/show-version.sh

#!/bin/bash
cd $(dirname $0)
privkey=${1:-../mycrpd.key}
INSTANCES=$(terraform output -json instances | jq -r 'to_entries[] | [.value.public_ip] | @tsv')
for ip in $INSTANCES; do
  ssh -i $privkey -o StrictHostKeyChecking=no root@$ip cat /etc/version
done
 ```

Example output:

```
$ stackpath/show-version.sh

 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC
 Linux client-world-fra-0 4.19.58 #1 SMP Thu Jul 18 18:10:18 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-fra-0 (10.128.0.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2568 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2660 v4 @ 2.00GHz
  _/_/_/  _/    _/  _/        _/_/_/     151.139.87.12 in Amsterdam,Netherlands,AS12989 StackPath LLC


 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC
 Linux client-world-lax-0 4.19.58 #1 SMP Thu Jul 18 18:10:18 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-lax-0 (10.128.64.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2567 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2695 v4 @ 2.10GHz
  _/_/_/  _/    _/  _/        _/_/_/     151.139.71.48 in Los Angeles,United States,AS12989 StackPath LLC


 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC
 Linux client-world-lhr-0 4.19.58 #1 SMP Thu Jul 18 18:10:18 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-lhr-0 (10.128.80.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2581 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2620 v2 @ 2.10GHz
  _/_/_/  _/    _/  _/        _/_/_/     151.139.43.48 in Amsterdam,Netherlands,AS12989 StackPath LLC


 RPD release 20.2R1.10 built by builder on 2020-06-25 12:51:12 UTC
 Linux client-world-nrt-0 4.19.58 #1 SMP Thu Jul 18 18:10:18 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    client-world-nrt-0 (10.128.96.2)
  _/        _/_/_/    _/_/_/    _/    _/   Cores 0, 2581 MBytes available
 _/        _/    _/  _/        _/   _/    Intel(R) Xeon(R) CPU E5-2620 v2 @ 2.10GHz
  _/_/_/  _/    _/  _/        _/_/_/     151.139.176.36 in Tokyo,Japan,AS33438 Highwinds Network Group, Inc.

```

# Destroy stack

There is cost associated with workloads deployed in the cloud, it is very important to clean up when
no longer required:

```
$ cd stackpath
$ terraform destroy
. . . 
Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

stackpath_compute_workload.client: Destroying... [id=8f24d413-b1cf-4270-95ce-4875635ea1f8]
stackpath_compute_workload.client: Destruction complete after 1s

Destroy complete! Resources: 1 destroyed.
```


