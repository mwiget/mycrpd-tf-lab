include .env
export

all: run

build: mycrpd/mycrpd.key.pub
	docker build -t mycrpd mycrpd

push: build docker-login
	docker tag mycrpd:latest ${CONTAINER_REGISTRY_USERNAME}/mycrpd:latest
	docker push ${CONTAINER_REGISTRY_USERNAME}/mycrpd:latest

docker-login:
	@docker login --username ${CONTAINER_REGISTRY_USERNAME} --password "${CONTAINER_REGISTRY_PASSWORD}" ${CONTAINER_REGISTRY}

mycrpd/mycrpd.key.pub:
	ssh-keygen -m PEM -N '' -f mycrpd.key && cp mycrpd.key.pub mycrpd/

run: build kill
	docker run -ti --rm -d --name mycrpd mycrpd:latest
	@sleep 1
	docker exec -ti mycrpd bash
	docker kill mycrpd

docker: build docker/.terraform
	cd docker && terraform apply -auto-approve
	./create_bgp_mesh.sh
	docker/show-version.sh

docker/.terraform:
	cd docker && terraform init

stackpath: stackpath/.terraform build push
	cd stackpath && terraform apply -auto-approve
	./create_bgp_mesh.sh stackpath
	stackpath/show-version.sh

stackpath/.terraform: stackpath/terraform.tfvars
	cd stackpath && terraform init

stackpath/terraform.tfvars:
	cd stackpath && cp terraform.tfvars.sample terraform.tfvars
	@echo "please set your stackid, usrname and password in stackpath/terraform.tfvars"

destroy: 
	-cd docker && terraform destroy -auto-approve
	-cd stackpath && terraform destroy -auto-approve

kill:
	-docker kill mycrpd

stop:
	-docker stop mycrpd

clean:
	-rm mycrpd.key* mycrpd/mycrpd.key.pub terraform.tfstate* */terraform.tfstate*
	--rm -rf .terraform */.terraform
