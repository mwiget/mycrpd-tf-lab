#!/bin/bash     

set -e

# augment banner with some system infos

MODEL=$(lscpu|grep name|sed 's/.*: *//')
CORES=$(lscpu|grep On-line|sed 's/.*: *//')
FREE=$(free -m  | grep ^Mem | awk '{print $7}')
VERSION=$(rpd -v)
IP=$(hostname -i)
PUBLICIP=$(curl -s https://ipinfo.io/ip)
LOCATION=$(curl -s http://ip-api.com/batch --data "[\"$PUBLICIP\"]" | jq -r ".[].city, .[].country, .[].as" | paste -sd ',' -)
UNAME=$(uname -a)

cat <<EOF > /etc/version

 $VERSION
 $UNAME

              _/_/_/    _/_/_/    _/_/_/
     _/_/_/  _/    _/  _/    _/  _/   _/    $HOSTNAME ($IP)
  _/        _/_/_/    _/_/_/    _/    _/   Cores $CORES, $FREE MBytes available
 _/        _/    _/  _/        _/   _/    $MODEL
  _/_/_/  _/    _/  _/        _/_/_/     $PUBLICIP in $LOCATION

EOF

cat <<EOF >> /usr/bin/crpd_banner
cat /etc/version
EOF

# enable ssh server
service ssh start

# load network settings from a script
if [ -s /config/network-init.sh ]; then
   /bin/bash /config/network-init.sh > /root/network-init.log 2>&1 & disown
else
  echo "no /config/network-init.sh file found" > /root/network-init.log
fi

# handoff to cRPD
export > /etc/envvars
exec /sbin/runit-init 0
