#!/usr/bin/env python3

import yaml
import json
import sys
import warnings
from jinja2 import Template
from jnpr.junos import Device
from jnpr.junos.utils.config import Config

# ignore CryptographyDeprecationWarning
warnings.filterwarnings(action='ignore', module='.*paramiko.*')

print("\nloading router ip addresses from stdin ...")

addresses = json.load(sys.stdin)

config = []

for router in addresses.keys():
    peers = addresses.copy()
    peers.pop(router)
    p = {}
    for peer in peers.keys():
        key = "neighbor_" + peer
        p[key] = {}
        p[key]['ip'] = peers[peer]["private_ip"]
    neighbors = {}
    neighbors['host_name'] = router
    neighbors['bgp'] = {}
    neighbors['bgp']['neighbors'] = p
    neighbors['bgp']['routes'] = { addresses[router]["public_ip"] + "/32" }
    config.append(neighbors)

#print(config)
#with open('t.yml', 'w') as f:
#    yaml.dump(config, f, default_flow_style=False)

fj = open('templates/bgp.j2')
j2 = fj.read()
template = Template(j2)
fj.close()

for device in config:
    conffilename = 'configs/'+device["host_name"]+'.conf.txt'
    print("rendering " + conffilename + " ...")
    conffile = open(conffilename, 'w')
    conffile.write(template.render(device))
    conffile.close()

print("\npush config change to devices ...")

for router in addresses.keys():
    ip = addresses[router]["public_ip"]
    print("push config to " + router + " (" + ip + ") ...")
    conffilename = 'configs/'+ router +'.conf.txt'

    dev = Device(host=ip, user='root',  ssh_private_key_file='./mycrpd.key')
    dev.open()
    cfg = Config(dev)
    cfg.rollback()  # Execute Rollback to prevent commit change from old config session
    cfg.load(path=conffilename, format='text')
    cfg.pdiff()
    # using udpate mode to keep the apply-group untouched on the device 
    if not cfg.commit():
        print('commit failed on ' + router)
    dev.close()

sys.exit()

#with open(r'bgp.yml') as file:
#    # The FullLoader parameter handles the conversion from YAML
#    # scalar values to Python the dictionary format
#    neighbors = yaml.load(file, Loader=yaml.FullLoader)
#    print(neighbors)
#    print(neighbors[0]['host_name'], ":", neighbors[0]['bgp']['neighbors']['neighbor_1']['ip'])
